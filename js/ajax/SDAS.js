/**
 * Created by sghimire on 4/21/2016.
 */
function currentEvents(){
    SDR.currentnews(currentEventsCallback());
}

function currentEventsCallback() {

    return function(status, data) {
        for(index in data.data) {
            var currentNews = data.data[index];
            var syntax = "data:image/gif;base64,";
            var imageString = currentNews.base64Value;
            var x= '<div class="world-news-grid">'+
                '<img src="'+syntax.concat(imageString)+'" alt="" />'+
                '<a href="singlepage.html" class="title">'+currentNews.title+' </a>'+
                '<p>'+currentNews.description+'</p>'+
                '<a href="singlepage.html">Read More</a>'+
                '</div>';
            $("#currentnews").append(x);
        }
    }
}
